package ictgradschool.industry.test02.q1;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public enum Direction {

    Up, Down, Left, Right, None
}
