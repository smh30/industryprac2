package ictgradschool.industry.test02.q1;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Starfield {

    private List<Star> stars;

    public Starfield(int width, int height) {

        stars = new ArrayList<Star>();
        for (int i = 0; i < 100; i++){
            int x = (int)(Math.random() * width);
            int y = (int)(Math.random() * height);
            int size = (int)(Math.random() * 5 + 2);
            int speed = (int)(Math.random() * 7 + 5);
            Star star = new Star(x, y, size, speed);
            stars.add(star);
        }

    }

    public void move(int windowWidth, int windowHeight) {
        for (Star star : stars) {
            star.move(windowWidth, windowHeight);
        }
    }

    public void paint(Graphics g) {
        for (Star star : stars) {
            star.paint(g);
        }
    }

}
