package ictgradschool.industry.test02.q1;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Spaceship {

    private static final int SPRITE_WIDTH = 100, SPRITE_HEIGHT = 100;

    private int x, y, width, height, speed;

    private Direction direction = Direction.Up;

    private Image image;

    public Spaceship(int x, int y, int speed) {

        try {
            this.image = ImageIO.read(new File("spaceship.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.width = SPRITE_WIDTH;
        this.height = SPRITE_HEIGHT;
        this.speed = speed;

    }

    public void move(Direction direction, int windowWidth, int windowHeight) {

        switch(direction){
            case None: return;
            case Up:
                y -= speed;
                if (y <= 0){
                    y = 0;
                }
                break;
            case Down:
                y += speed;
                if (y+height >= windowHeight){
                    y = windowHeight-height;
                }
                break;
            case Right:
                x += speed;
                if (x+width >= windowWidth){
                    x = windowWidth-width;
                }
                break;
            case Left:
                x -= speed;
                if (x <= 0){
                    x = 0;
                }
                break;
        }
        this.direction = direction;

    }

    public void paint(Graphics g) {
        if (direction == Direction.Up) {
            g.drawImage(image, x, y, x + width, y + height, 0, 0, width, height, null);
        } else if (direction == Direction.Down){
            g.drawImage(image, x, y, x + width, y + height, width, 0, width *2, height, null);
        } else if (direction == Direction.Left){
            g.drawImage(image, x, y, x + width, y + height, width *2, 0, width *3, height, null);
        } else if (direction == Direction.Right){
            g.drawImage(image, x, y, x + width, y + height, width *3, 0, width *4, height, null);
        }


    }

}
