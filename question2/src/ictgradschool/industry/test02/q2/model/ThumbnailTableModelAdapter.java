package ictgradschool.industry.test02.q2.model;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class ThumbnailTableModelAdapter extends AbstractTableModel implements ThumbnailListener {

    private static final String[] COLUMN_NAMES = {"Image", "Name"};
    ThumbnailList thumbnails;


    public ThumbnailTableModelAdapter(ThumbnailList thumbnails) {
        this.thumbnails = thumbnails;
        thumbnails.addListener(this);

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0: return thumbnails.get(rowIndex).getImage();
            case 1: return thumbnails.get(rowIndex).getName();
        }
        return null;
    }

    @Override
    public int getRowCount() {
        return thumbnails.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    /**
     * Sets the table up to display images in its first column, and Strings in its second column.
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return ImageIcon.class;
            case 1:
                return String.class;
            default: throw new IllegalArgumentException();
        }
    }

    /**
     * Sets the table up with the correct column names.
     */
    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public void thumbnailAdded(ThumbnailList list) {
        fireTableDataChanged();

    }

    @Override
    public void thumbnailListCleared(ThumbnailList list) {

    }
}
